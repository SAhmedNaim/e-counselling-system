<?php

    $filepath = realpath(dirname(__FILE__));
    include_once ($filepath.'/../lib/Session.php');
    Session::init();

    if (isset($_GET['action']) && $_GET['action'] == "logout") 
    {
        Session::destroy();
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>E-Counselling System</title>

    <!-- Bootstrap -->
    <link href="inc/css/bootstrap.min.css" rel="stylesheet">
    <!-- <link href="inc/css/stylestudent.css" rel="stylesheet"> -->
    <link href="inc/css/style.css" rel="stylesheet">
<body>

    <!-- Navigation Bar Start -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">E-Counselling System</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <form class="navbar-form navbar-left" action="search.php" method="get">
                    <div class="form-group">
                        <input type="text" class="form-control" id="searchBar" name="search" placeholder="Search">
                    </div>
                    <!-- <button type="submit" class="btn btn-default glyphicon glyphicon-search" name="search">Search</button> -->
                    <input type="submit" class="btn btn-default glyphicon glyphicon-search" value="Search" id="searchButton" />
                </form> 
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php
                            if(Session::get('type')==1) echo 'student.php';
                            elseif(Session::get('type')==2) echo 'teacher.php';
                        ?>">Home</a></li>
                    <li><a href="https://daffodilvarsity.edu.bd/department/swe">Notice Board</a></li>
                    <li><a href="https://daffodilvarsity.edu.bd/">DIU Website</a></li>
                    <li><a href="https://www.facebook.com/profile.php?id=19910327202&ref=br_rs">DIU News</a></li>
                    
                    <?php 
                        $userlogin = Session::get("login");
                        if ($userlogin == true) 
                        { ?>
                            <li><a href="profile.php">Profile</a></li> 
                            <li><a href="?action=logout">Logout</a></li> <?php
                        } 
                    ?>

                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <!-- Navigation Bar End -->
    