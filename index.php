<?php
    include_once 'inc/header.php';
?>

    <div class="wrap">
        <h1>Let's Solve Together</h1>
        <a href="login.php" class="btn btn-success">Login</a>
    </div>

    <div class="events">
        <h2>UPCOMING EVENT</h2>

        <div class="col-md-3">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="thumbnail">
                        <img src="inc/img/tiger.jpg" alt="Image Missing"/>
                        <div class="caption">
                            <h3>Saint Martin Tour</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            <!-- <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="thumbnail">
                        <img src="inc/img/computer.jpg" alt="Image Missing"/>
                        <div class="caption">
                            <h3>SUST CSE Festival</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            <!-- <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-3">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="thumbnail">
                        <img src="inc/img/contest.jpg" alt="Image Missing"/>
                        <div class="caption">
                            <h3>Programming Contest</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            <!-- <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="thumbnail">
                        <img src="inc/img/ict.jpg" alt="Image Missing"/>
                        <div class="caption">
                            <h3>Daffodil ICT Carnival</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            <!-- <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

<?php include_once 'inc/footer.php'; ?>
