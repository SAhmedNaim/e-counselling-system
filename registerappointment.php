<?php
    include_once 'inc/header.php';
    Session::checkSession();

    include_once 'lib/User.php';

    $user = new User;

    if(isset($_POST['submit']))
    {
        $appointmentTeacher = $user->appointmentTeacher($_POST);
    }

?>

<div class="content">

    <form action="" method="post">

        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8" style="background-color: white; height: 350px;">
                <div class="col-md-6">
                    <h2>PROBLEM TOPIC</h2>
                    <textarea rows="9" cols="57" style="background-color: #e3e3e3;" name="topic"></textarea>
                    <label for="dates">CHECK DATE AGAIN</label>
                    <br/>
                    <input type="date" id="dates" name="dates" class="form-control" />
                </div>
                <div class="col-md-6">
                    <h2>DESCRIBE YOUR PROBELM</h2>
                    <textarea rows="9" cols="55" style="background-color: #e3e3e3;" name="description"></textarea>
                    <label for="date">CHECK TIME</label>
                    <br/>
                    <input type="time" id="time" name="time" class="form-control" />
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>

        <div class="row">
            <div class="col-md-8"></div>
            <div class="col-md-4">
                <a href="index.php" class="btn btn-default" style="margin-left: 44px; margin-top: 15px;">CANCEL</a>
                <!-- <a href="congratulation.php" class="btn btn-default" style="margin-left: 8px; margin-top: 15px;">OK</a> -->
                <input type="submit" name="submit" class="btn btn-default" style="margin-left: 8px; margin-top: 15px;" value="OK" id="ok" />
            </div>
        </div>

    </form>

</div>

<?php include_once 'inc/footer.php'; ?>
