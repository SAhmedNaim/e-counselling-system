<?php
    include_once 'inc/header.php';
    Session::checkSession();

    include_once 'lib/User.php';

    $user = new User;

    $userData = $user->getUserById();
?>

    <div class="content">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6" style="background-color: #e3e3e3;">
                <div class="col-md-5">
                    <div class="row" style="margin-top: 22px;">
                        <div class="col-xs-12 col-md-12">
                            <a href="#" class="thumbnail">
                                <img src="inc/img/<?php echo $userData->photo; ?>" alt="User photo"/>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <input type="text" name="" value="<?php echo $userData->name; ?>" id="registerField" readonly />
                    <input type="text" name="" value="<?php echo $userData->diu_id; ?>" id="registerField"  readonly />
                    

                    <input type="text" name="" value="<?php echo $userData->dept; ?>" id="registerField"  readonly />

                    <input type="text" name="" value="<?php echo $userData->phone; ?>" id="registerField"  readonly />
                    <input type="text" name="" value="<?php
                            if($userData->type == 1) echo 'Student';
                            elseif($userData->type == 2) echo 'Teacher';
                        ?>
                    " id="registerField" readonly />

                    <!-- <div id="registerField">
                        <input type="file" class="form-control-file">
                    </div> -->

                    <a href="index.php" class="btn btn-default" style="padding: 10px 40px; margin-left: 0px; margin-top: 15px; margin-bottom: 20px;">OK</a>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>

    
    
<?php include_once 'inc/footer.php'; ?>
