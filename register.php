<?php
    include_once 'inc/header.php';
    include_once 'lib/User.php';
    Session::checkLogin();

    $user = new User;

    if (isset($_POST['register'])) 
    {
        $userRegistration = $user->userRegistration($_POST);
    }

    if(isset($userRegistration))
    {
        echo $userRegistration;
    }

?>

<div class="content">
    <div class="register_form">
        <form action="" method="post" class="navbar-form navbar-left" role="search">

            <div class="form-group">
                <input type="text" width="100%" class="form-control" placeholder="Enter Your DIU ID" name="diu_id">
            </div>

            <div class="form-group">
                <input type="text" width="100%" class="form-control" placeholder="Enter name here..." name="name">
            </div>

            <div class="form-group">
				<select class="form-control" id="inputField" name="dept">
					<option value="">Select department</option>
					<option value="cse">CSE</option>
					<option value="swe">SWE</option>
					<option value="eee">EEE</option>
				</select>
			</div>

            <div class="form-group">
				<select class="form-control" id="inputField" name="type">
					<option value="">Select user type...</option>
					<option value="1">Student</option>
					<option value="2">Teacher</option>
				</select>
			</div>

            <div class="form-group">
                <input type="password" width="100%" class="form-control" placeholder="Password" name="password" />
            </div>
            
            <input type="submit" class="btn btn-success" value="Register" name="register" />

        </form>
    </div>
</div>
    
<?php include_once 'inc/footer.php'; ?>
