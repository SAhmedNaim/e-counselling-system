<?php

include 'Database.php';

class User 
{
	private $db;

	public function __construct() 
	{
		$this -> db = new Database();
	}

	public function userRegistration($data) 
	{
		$diu_id 	= $data['diu_id'];
		$name 		= $data['name'];
		$dept		= $data['dept'];
		$type		= $data['type'];
		$password	= $data['password'];

		$chk_diuID	= $this->diuID_Check($diu_id);

		if ($diu_id == "" OR $name == "" OR $dept == "" OR $type == "" OR $password == "") 
		{
			$msg = "<div class='alert alert-danger'><strong>Error! </strong>Field must not be empty!</div>";
			return $msg;
		}

		if (strlen($password) < 3) 
		{
			$msg = "<div class='alert alert-danger'><strong>Error! </strong>Password must be more than 3 digit long!</div>";
			return $msg;
		}		

		if ($chk_diuID == true) {
			$msg = "<div class='alert alert-danger'><strong>Error! </strong>User already exists!</div>";
			return $msg;
		}

		$password = md5($data['password']);

		$sql = "INSERT INTO user(diu_id, name, dept, type, password) 
				VALUES(:diu_id, :name, :dept, :type, :password)";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':diu_id', $diu_id);
		$query->bindValue(':name', $name);
		$query->bindValue(':dept', $dept);
		$query->bindValue(':type', $type);
		$query->bindValue(':password', $password);
		$result = $query->execute();
		if ($result) 
		{
			$msg = "<div class='alert alert-success'><strong>Success! </strong>Thank you, You have been registered.</div>";
			return $msg;
		} 
		else 
		{
			$msg = "<div class='alert alert-danger'><strong>Error! </strong>Sorry! There has been problem inserting your details.</div>";
			return $msg;
		}

	}

	public function diuID_Check($diu_id) 
	{
		$sql = "SELECT diu_id FROM user WHERE diu_id = :diu_id";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':diu_id', $diu_id);
		$query->execute();
		if ($query->rowCount() > 0) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}

	public function getLoginUser($diu_id, $password) 
	{
		$sql = "SELECT * FROM user WHERE diu_id = :diu_id AND password = :password LIMIT 1";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':diu_id', $diu_id);
		$query->bindValue(':password', $password);
		$query->execute();
		$result = $query->fetch(PDO::FETCH_OBJ);
		return $result;
	}

	public function userLogin($data) 
	{
		$diu_id		= $data['diu_id'];
		$password	= $data['password'];

		$chk_diuID	= $this->diuID_Check($diu_id);

		if ($diu_id == "" OR $password == "") 
		{
			$msg = "<div class='alert alert-danger'><strong>Error! </strong>Field must not be empty!</div>";
			return $msg;
		}

		if ($diu_id == false) 
		{
			$msg = "<div class='alert alert-danger'><strong>Error! </strong>Email address not exists!</div>";
			return $msg;
		}

		$password = md5($data['password']);
		
		$result = $this->getLoginUser($diu_id, $password);

		if ($result) 
		{
			Session::init();
			Session::set("login", true);
			Session::set("id", $result->id);
			Session::set("diu_id", $result->diu_id);
			Session::set("name", $result->name);
			Session::set("dept", $result->dept);
			Session::set("type", $result->type);
			Session::set("photo", $result->photo);
			Session::set("loginmsg", "<div class='alert alert-success'><strong>Success! </strong>You are loggedIn!</div>");

			if(Session::get('type') == 1)
			{
				header("Location: student.php");
			}
			elseif(Session::get('type') == 2) 
			{
				header("Location: teacher.php");
			}

		} 
		else 
		{
			$msg = "<div class='alert alert-danger'><strong>Error! </strong>Data not found!</div>";
			return $msg;
		}

	}

	public function getUserById() 
	{
		$sql = "SELECT * FROM user WHERE diu_id = :diu_id LIMIT 1";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':diu_id', Session::get('diu_id'));
		$query->execute();
		$result = $query->fetch(PDO::FETCH_OBJ);
		return $result;
	}

	public function userTask()
	{
		$sql = "SELECT * FROM task WHERE user_id = :id AND date = :date;";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':id', Session::get('id'));
		$query->bindValue(':date', date('Y-m-d'));
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}

	public function todayStudentMeeting()
	{
		// $sql = "SELECT * FROM appointment WHERE student_id = :id AND dates = :dates;";
		$sql = "SELECT * FROM appointment 
				JOIN user
				ON 
				user.id = appointment.teacher_id 
				WHERE student_id = :id AND dates = :dates;";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':id', Session::get('id'));
		$query->bindValue(':dates', date('Y-m-d'));
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}

	public function todayTeacherMeeting()
	{
		// $sql = "SELECT * FROM appointment WHERE teacher_id = :id AND dates = :dates;";
		$sql = "SELECT * FROM appointment 
				JOIN user
				ON 
				user.id = appointment.student_id 
				WHERE teacher_id = :id AND dates = :dates;";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':id', Session::get('id'));
		$query->bindValue(':dates', date('Y-m-d'));
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}

	public function allTeacherMeeting()
	{
		// $sql = "SELECT * FROM appointment WHERE teacher_id = :id;";
		$sql = "SELECT * FROM appointment 
				JOIN user
				ON 
				user.id = appointment.student_id 
				WHERE teacher_id = :id;";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':id', Session::get('id'));
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}

	public function studentCourse()
	{
		$sql = "SELECT * FROM student_course 
				JOIN course 
				ON student_course.course_id = course.id
				WHERE student_id = :id;";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':id', Session::get('id'));
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}

	public function search($name)
	{
		$sql = "SELECT* FROM user WHERE type = 2 AND name LIKE '%$name%' LIMIT 1;";
		$query = $this->db->pdo->prepare($sql);
		$query->execute();
		$result = $query->fetch(PDO::FETCH_OBJ);
		return $result;
	}

	public function appointmentTeacher($data)
	{
		$student_id 	= Session::get('id');
		$teacher_id 	= Session::get('teacher_id');
		$topic 			= $data['topic'];
		$description 	= $data['description'];
		$dates 			= $data['dates'];
		$time 			= $data['time'];

		$sql = "INSERT INTO appointment (student_id, teacher_id, topic, description, time, dates)
    			VALUES (:student_id, :teacher_id, :topic, :description, :time, :dates);";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':student_id', $student_id);
		$query->bindValue(':teacher_id', $teacher_id);
		$query->bindValue(':topic', $topic);
		$query->bindValue(':description', $description);
		$query->bindValue(':time', $time);
		$query->bindValue(':dates', $dates);
		$query->execute();
		header('Location:congratulation.php');
	}


}
