<?php
    include_once 'inc/header.php';
    Session::checkSession();

    include_once 'lib/User.php';

    $user = new User;

    $userData = $user->getUserById();

    $userTask = $user->userTask();

    $todayTeacherMeeting = $user->todayTeacherMeeting();

    $allTeacherMeeting = $user->allTeacherMeeting();

    if($userData->type == 1) 
    {
        header('Location:student.php');
    }
?>

    <div class="content" style="color: white; font-size: 20px;">
        <div class="row">
            <div class="col-md-6" style="height: 220px; width: 44%; background-color: #00796B; margin-right: 3%; margin-left: 3%;">
                
                <div class="row">
                    <div class="col-xs-4 col-md-4" style="margin-top: 15px;">
                        <a href="#" class="thumbnail">
                            <img src="inc/img/<?php echo $userData->photo; ?>" alt="Image Missing"/>
                        </a>
                    </div>

                    <div class="col-md-8" style="margin-top: 15px;">
                        <?php echo $userData->name; ?>
                        <br/>
                        <?php echo $userData->diu_id; ?>
                        <br/>
                        <?php echo $userData->phone; ?>
                        <br/>
                        Dept. of <?php echo $userData->dept; ?>
                        <br/>
                        Daffodil International University.
                    </div>
                </div>

            </div>
            <div class="col-md-6" style="height: 220px; width: 45%; background-color: #00796B;">
                <h3>Date: <?php echo date('d-M-Y'); ?></h3>
                <h4 style="padding: 10px; background-color: #00695C;">Important tasks to do</h4>
                <table>
                    <?php 
                        if($userTask) 
                        {
                            foreach ($userTask as $task) 
                            { ?>
                                <tr>
                                    <td><?php echo $task['title']; ?>: </td>
                                    <td><?php echo $task['time']; ?></td>
                                </tr><?php
                            }
                        }
                    ?>
                </table>
            </div>
        </div>
        <div class="row" style="margin-top: 10px;">
        
            <div class="col-md-6" style="width: 44%; margin-left: 3%; margin-right:3%; background-color: #00796B;">
                Meeting with
                <table class="table table-bordered">
                    <tr>
                        <td>Name</td>
                        <td>Problem Topic</td>
                        <td>Date</td>
                    </tr>
                    
                    <?php 
                        if($allTeacherMeeting)
                        {
                            foreach ($allTeacherMeeting as $value) 
                            { ?>
                                <tr>
                                    <td><?php echo $value['name']; ?></td>
                                    <td><?php echo $value['topic']; ?></td>
                                    <td><?php echo $value['dates']; ?></td>
                                </tr> <?php
                            }
                        }
                    ?>
                    

                </table>
            </div>
            
            <div class="col-md-6" style="width: 45%; background-color: #00796B;">
                Today meeting with
                <table class="table table-bordered">
                    <tr>
                        <td>Name</td>
                        <td>Problem Topic</td>
                        <td>Time</td>
                    </tr>
                    
                    <?php 
                        if($todayTeacherMeeting)
                        {
                            foreach ($todayTeacherMeeting as $value) 
                            { ?>
                                <tr>
                                    <td><?php echo $value['name']; ?></td>
                                    <td><?php echo $value['topic']; ?></td>
                                    <td><?php echo $value['time']; ?></td>
                                </tr> <?php
                            }
                        }
                    ?>

                </table>
            </div>
        </div>
    </div>

    
    
<?php include_once 'inc/footer.php'; ?>
