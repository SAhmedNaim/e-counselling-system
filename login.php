<?php
    include_once 'inc/header.php';
    include_once 'lib/User.php';
    Session::checkLogin();

    $user = new User;

    if (isset($_POST['login'])) 
    {
        $userLogin = $user->userLogin($_POST);
    }

    if(isset($userLogin))
    {
        echo $userLogin;
    }
?>

    <div class="login">
        <h1>Login with your DIU-ID</h1>
        <div class="login_form">
            <form action="" method="post" class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" width="100%" class="form-control" placeholder="Enter Your ID" name="diu_id">
                </div>
                <div class="form-group">
                    <input type="password" width="100%" class="form-control" placeholder="Password" name="password">
                </div>
                <input type="submit" class="btn btn-success" value="Login" name="login" />
            </form>
        </div>
    </div>

    
    
<?php include_once 'inc/footer.php'; ?>
