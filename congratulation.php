<?php
    include_once 'inc/header.php';
    Session::checkSession();
?>


    <div class="content">
        <div class="row" style="margin-top: 150px;">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>Congratulations</h2>
                    </div>
                    <div class="panel-body">
                        <h4>You have successfully make an appointment.</h4>
                    </div>
                    <a href="student.php" class="btn btn-default" style="width: 90px; margin-left: 320px; margin-bottom: 15px;">OK</a>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>

    
<?php include_once 'inc/footer.php'; ?>
