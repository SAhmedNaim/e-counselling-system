<?php
    include_once 'inc/header.php';
    Session::checkSession();

    include_once 'lib/User.php';

    $user = new User;

    if(isset($_GET['search']))
    {
        $search = $user->search($_GET['search']);
    }

    if(isset($_GET['appointment']))
    {
        Session::set('teacher_id', $_GET['appointment']);
        header("Location:registerappointment.php");
    }

?>

<div class="content">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6" style="background-color: white; padding-bottom: 220px;">
            <?php 
                if (isset($search->id)) 
                { ?>
                    <h2 style="background-color: #e3e3e3; padding:30px;"><?php echo $search->name; ?></h2>
                    <br/>
                    <h4 style="background-color: #e3e3e3; padding:20px;"><?php echo $search->name; ?>
                    <a href="?appointment=<?php echo $search->id; ?>" class="btn btn-default" name="appointment" style="margin-left: 280px;">Get Appointment</a>
                    </h4> <?php
                }
                else
                {
                    echo '<h2 style="text-align:center;">Data not found!!!</h2>';
                }
            ?>
            
        </div>
        <div class="col-md-3"></div>
    </div>
</div>


    
<?php include_once 'inc/footer.php'; ?>
